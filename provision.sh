#!/bin/env bash

# アップデート
yum update -y
# vim を使えるようにする
yum install -y vim ntp bridge-utils NetworkManager

# private_network 設定(/etc/sysconfig/network-scripts/ifcfg-enp0s* が生成される)
# インタフェース名
nmcli c mod enp0s8 connection.interface-name "enp0s8"
# IPアドレスとレンジ
nmcli c mod enp0s8 ipv4.addresses "192.168.30.10/24"
# DHCPを利用しない
nmcli c mod enp0s8 ipv4.method "manual"
# OS立ち上げ時自動起動
nmcli c mod enp0s8 connection.autoconnect "yes"
# インターフェイス起動
nmcli d connect enp0s8

# IPv4 forwardingの設定
sysctl -w net.ipv4.ip_forward=1
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf

# Dockerをインストール
curl -fsSL https://get.docker.com/ | sh
# user = vagrant でdockerコマンドをsudoを付けなくても実行できるようにする
usermod -aG docker vagrant

# Docker-composeをインストール(参考：https://docs.docker.com/compose/install/)
curl -L "https://github.com/docker/compose/releases/download/1.11.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
